This is a small rails app I wrote for myself for learning my English vocabulary.

![Screenshot: Lessons View](https://bitbucket.org/timjb/vokabeln/raw/21b258ca1fb6/public/images/screenshot1.png)

![Screenshot: Vocables View](https://bitbucket.org/timjb/vokabeln/raw/21b258ca1fb6/public/images/screenshot2.png)
